#!/bin/bash
export COMMANDS='!hiragana$\|!katakana$\|!furigana$\|!about$'
while  [ 1=1 ]
do
		export	INPUT=$(cat $FIFO/out | tail -1 | grep  $COMMANDS)
		export  TEST=$(echo $INPUT | grep -o $COMMANDS)
		if [ -n "$INPUT" ]
			then
			case $TEST in
			'!hiragana')
				echo $INPUT | sed 's/.\{9\}$//' |  kakasi -i utf8 -o utf8 -JH > $FIFO/in
			;;
			'!katakana')
				echo $INPUT | sed 's/.\{9\}$//' |  kakasi -i utf8 -o utf8 -JK > $FIFO/in
			;;
			'!furigana')
				echo $INPUT | sed 's/.\{9\}$//' |  kakasi -i utf8 -o utf8 -JHf > $FIFO/in
			;;
			'!about')
				echo "Hello! This is Kanabot, written by infiniteNOP. I am a ~20-line long bash script. I am licensed under the GNU GPL. My code can be found at https://bitbucket.org/linuxlalala/kanabot/src. I use kakasi." > $FIFO/in
			esac
		fi
done